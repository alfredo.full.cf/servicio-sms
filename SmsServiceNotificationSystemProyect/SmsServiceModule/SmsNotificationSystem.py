import paho.mqtt.client as mqtt # importando al cliente
import datetime
import time

from utility.database import DataBasev2 as DB
from utility.encrypt import EncodeDecodeMessage as EDM
from utility.sim900 import sim900 

global port 

broker_address="broker.mqtt-dashboard.com"
topics = ["in_sms_security"]

table_token = 'Tokens'
table_sms_notification = 'SmsNotification'


token_entrance = 'token_entrance'

path_data_base = '/home/pi/Documents/SmsServiceNotificationSystemProyect/DataBase/SERVICESMS.db'

def on_connect(client, userdata, flags, rc):
    print("Connected!", str(rc))
    time.sleep(1)
    client.subscribe(topics[0])
    
def on_message(client, userdata, message):
    if '/*/' in message.payload.decode("utf-8"):
        messageSplit = message.payload.split('/*/')
        
        tokenSha256 = messageSplit[0]
        phoneNumber = messageSplit[1]
        message = messageSplit[2]
        
        con = DB.sql_connection(path_data_base)
        salt = DB.sql_search_data(con, token_entrance,table_token,messageSplit[0])
        if(salt):
            phoneNumberDecode = EDM.decrypt_with_AES(phoneNumber,tokenSha256,salt)
            messageDecode = EDM.decrypt_with_AES(message,tokenSha256,salt)
            
            registrationDate = datetime.datetime.now()
            date_time = registrationDate.strftime("%m/%d/%Y %H:%M:%S")
            
            columns = '(id integer PRIMARY KEY AUTOINCREMENT, token_entrance TEXT NOT NULL, phone_number TEXT NOT NULL, message TEXT NOT NULL, date_registered TEXT NOT NULL)'
            DB.sql_table(con,table_sms_notification,columns)
            
            columnsInsert = ('token_entrance', 'phone_number', 'message', 'date_registered')
            data = (tokenSha256,str(phoneNumberDecode),str(messageDecode),date_time)            
            DB.sql_insert(con, table_sms_notification, columnsInsert, data)
            
            
            print("Inserted Data...")
            
            sim900.send_message(port,str(phoneNumberDecode),str(messageDecode))
            
            
        con.close()  


port = sim900.connect()
sim900.setup(port)

client = mqtt.Client("client_service_sms_notification")
client.connect(broker_address)
time.sleep(1)
client.on_connect = on_connect
client.on_message = on_message
time.sleep(1)

client.loop_forever()




