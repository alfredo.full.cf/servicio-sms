import sqlite3
   
def sql_connection(database):
    try:
        con = sqlite3.connect(database)
        return con
    except Error:
        print(Error)
        
def sql_table(con,table,columns):
    query = "CREATE TABLE IF NOT EXISTS {0} {1} ".format(table, columns)
    cursorObj = con.cursor()
    cursorObj.execute(query)
    con.commit()
    
def sql_insert(con, table, columns, data):
    query = "INSERT INTO {0} {1} VALUES {2}".format((table),(columns),(data))
    cursorObj = con.cursor()
    cursorObj.execute(query)
    con.commit()
    
def sql_update(con,table,column,data,condition1,condition2):
    query = "UPDATE {0} SET {1} = {2} WHERE {3} = {4}".format(table,column,data,condition1,condition2)
    cursorObj = con.cursor()
    cursorObj.execute(query)
    con.commit()
    
def sql_fetch(con,data,table):
    query = "SELECT {0} FROM {1}".format(data,table)
    cursorObj = con.cursor()
    cursorObj.execute(query)
    rows = cursorObj.fetchall()
    for row in rows:
        print(row)
        
def sql_search_data(con, column,table, data):
    query = "SELECT token_entrance , salt FROM {1} WHERE {0} = '{2}'".format(column,table, data)
    cursorObj = con.cursor()
    cursorObj.execute(query)
    rows = cursorObj.fetchall()
    for row in rows:
        if(str(row[0]) == data):
            return row[1]
    return ''    