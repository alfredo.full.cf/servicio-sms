**Diseño e implementación de un sistema de notificaciones por medio de un servicio de mensajería 
de texto encriptado para la aplicación chapamas**

El siguiente proyecto tiene como finalidad crear un servicio de mensajeria de texto para notificar a los motorizados sobre nuevos pedidos.
Creado en raspberry pi utilizando el lenguaje python 

**Diseño**
![Screenshot](Diseño/Diseño Raspberry pi + sim900 gsm.png) 

en la siguietne imagen podemos ver como se conecta el componente SIM900 al raspberry pi 3

**Iniciando Sim900 en raspberr pi**

para que raspberry pi detecte a sim900 gsm por medio de RX y TX iniciaremos una consola y podremos los siguiente


> pi@raspberrypi: $ cd /dev                                      
> pi@raspberrypi: $ ls -l


buscaremos el serial conectado en RX TX que se llama:

-   Serial -> ttyAMA0 (en raspberry pi 2)
-   Serail -> ttyS0 (en raspberry pi3 +)

Si no existe el Serial para tu Raspberry pi
cambiamos la configuracion por defecto.


> pi@raspberrypi: $ sudo nano /boot/config.txt

agregamos una linea de codigo al final del archivo
```
#enable_uart=1
```
guardamos y reiniciamos.

"NOTA: encaso de que no podamos usar el puerto ttyS0 por tener un acceso denegado lo masfactible seria usar los siguientes
codgio para poder habilitarlo"
```
groups ${USER}
sudo gpasswd --add ${USER} dialout
```


> pi@raspberrypi: $ sudo reboot

despues de reiniciar debe de aparecerte el Serial en los puertos /dev 

Instalamos el terminal PICOCOM para usar el sim900 con comandos AT.

> pi@raspberrypi: $ sudo apt-get install picocom                                                                          
> pi@raspberrypi: $ picocom --baud 9600 /dev/ttyS0                                                                                                     
> AT                                                                                                                                   
> OK                              

iniciamos el picocom y escribimos AT, nos debe responder con un OK 

**implementación de Sim900 en pthon:**
```
import serial
import RPi.GPIO as GPIO   
import time  

SERIAL_PORT = "/dev/ttyS0"
# Enable Serial Communication
port = serial.Serial(SERIAL_PORT, baudrate = 9600, timeout = 5)
port.write("AT")
```

*Iniciar sim900 en modo mensajes:*

```
def setup():
    port.write('AT'+'\r')#comando AT = OK
    time.sleep(1)
    port.write('ATE0'+'\r')#comando AT0 = OK
    time.sleep(1)
    port.write('AT+CMGF=1'+'\r')#comando sim900 en modo mensajes
    time.sleep(1)
    port.write('AT+CNMI=2,1,0,0,0'+'\r')#comando sim900 recibir mensaje
    time.sleep(1)
```
*Codigo para enviar Mensaje desde Sim900:*

```
def send_message(phoneNumber, message):
    port.write('AT+CMGS="'+phoneNumber+'"'+'\r')#comando para enviar mensajes de texto
    time.sleep(0.5)
    port.write(message+'\r')#mensaje
    time.sleep(0.5)
    port.write("\x1a")#ctrl + z
    time.sleep(2)
```


****Base de datos SQL LITE en Raspberry pi 3****

*Instalacion de un SQL lite*

> pi@raspberry:~ $ sudo apt-get install sqlite3

presionamos Y y enter

*Creacion de una base de datos*

> pi@raspberry:~ $ sqlite3 sensordata.db

**Sql lite + raspberry por python**

*Iniciando SQL LITE3 en python*

```
import sqlite3

dbase = sqlite3.connect('base_de_datos.db')#open database file
cursor = dbase.cursor()
```

*funciones sql lite 3 en pyhon*

```
def create_table():
    dbase.execute(''' CREATE TABLE IF NOT EXISTS nombre_tabla(
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    lastname TEXT NOT NULL''')

    print 'Table created'
    
def read_Data():
    # from math import *
    data = cursor.execute(''' SELECT * FROM nombre_tabla ''')
    for record in data:
        print 'ID : '+str(record[0])
        print 'NAME : '+str(record[1])
        print 'LASTNAME : '+str(record[2])
        
def insert_Data(NAME, LASTNAME):
    data = "INSERT INTO nombre_tabla(NAME,LASTNAME) VALUES(?,?)"
        
    dbase.execute(data, (NAME,LASTNAME))
    
    dbase.commit()
    print 'Record inserted'
```
**Mosquito (MQTT) en raspberry pi 3**

Se utilizara el protocolo mosquito para la comunicacion de los servicios entre python y java.
 Por medio de los topicos asignados por la empresa.

*La clase Paho-MQTT*
para instalar Paho-Mqtt en nuestro raspberry pi3 se utiliza el siguietne comando.

> pi@raspberrypi: $ pip install paho-mqtt

una ves tengamos instalado podremos llamarlo desde python:
```
import paho.mqtt.client as paho # importando al cliente

broker_address="broker.hivemq.com" #Servidor mosquito gratis

def on_connect(client, userdata, flags, rc):
    print("Connected!", str(rc))
    time.sleep(1)
    client.subscribe("topic") //topico para la comunicacion

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

client = paho.Client("nombre_de_cliente")
client.connect(broker_address)

client.on_connect = on_connect
client.on_message = on_message

client.loop_forever()
```




