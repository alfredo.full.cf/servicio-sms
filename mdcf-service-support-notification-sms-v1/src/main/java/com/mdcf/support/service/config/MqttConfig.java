package com.mdcf.support.service.config;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;

@Configuration
public class MqttConfig {

    private static String mqttClient = MqttClient.generateClientId();

//    @Bean(name = "mqttInbound")
//    public IntegrationFlow mqttInbound() {
//        return IntegrationFlows.from(
//                new MqttPahoMessageDrivenChannelAdapter("tcp://broker.mqtt-dashboard.com",
//                        mqttClient, "1n_M3ssag3_xA17E19D0"))
//                .handle(m -> System.out.println("inBound:" + m.getPayload()))
//                .get();
//    }


    @Bean(name = "mqttOutbound")
    public IntegrationFlow mqttOutbound() {
        return f -> f.handle(new MqttPahoMessageHandler("tcp://broker.mqtt-dashboard.com", mqttClient));
    }
}
