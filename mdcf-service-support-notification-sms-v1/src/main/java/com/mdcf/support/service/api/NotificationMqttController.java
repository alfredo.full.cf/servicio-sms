package com.mdcf.support.service.api;

import com.mdcf.core.model.base.ResponseUtil;
import com.mdcf.support.service.util.EncryptUtil;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "NotificationMqttController")
@Slf4j
@RestController
@RequestMapping("/support/notification/sms/v1")
@RequiredArgsConstructor
public class NotificationMqttController {

    @Autowired
    @Qualifier("mqttOutbound")
    private IntegrationFlow mqttConfig;

    @GetMapping(value = "/create/token", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseUtil> tokenCreation(
            @RequestParam String token,
            @RequestParam String salt,
            @RequestParam String user) throws Exception {
        String cipherUser = EncryptUtil.encrypt(EncryptUtil.secretKey, salt, user);
        String tokenEncode = EncryptUtil.messageDigest(token);
        String message = tokenEncode + "/))*" + salt + "(((**/" + cipherUser;

        log.info("MESSAGE: {}", message);
        Message<String> helloMessage =
                MessageBuilder.withPayload(message)
                        .setHeader(MqttHeaders.TOPIC, "in_token_security")
                        .build();
        mqttConfig.getInputChannel().send(helloMessage);
        return ResponseEntity.ok(ResponseUtil.builder().success(true).build());
    }

    @GetMapping(value = "/send/sms", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ResponseUtil> sendSms(
            @RequestParam String token,
            @RequestParam String salt,
            @RequestParam String phoneNumber,
            @RequestParam String message) throws Exception {
        String cipherPhoneNumber = EncryptUtil.encrypt(EncryptUtil.secretKey, salt, phoneNumber);
        String cipherPhoneMessage = EncryptUtil.encrypt(EncryptUtil.secretKey, salt, message);
        String tokenEncode = EncryptUtil.messageDigest(token);
        String encryptMessage = tokenEncode + "***(" + salt + "/**" + cipherPhoneNumber + "///" + cipherPhoneMessage;


        log.info("MESSAGE: {}", encryptMessage);
        Message<String> helloMessage =
                MessageBuilder.withPayload(encryptMessage)
                        .setHeader(MqttHeaders.TOPIC, "in_sms_security")
                        .build();
        mqttConfig.getInputChannel().send(helloMessage);
        return ResponseEntity.ok(ResponseUtil.builder().success(true).build());
    }

}
