package com.mdcf.support.service;

import com.mdcf.core.starter.oauth.client.config.EnableStarterOauthClient;
import com.mdcf.core.starter.webmvc.runner.StarterWebApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableStarterOauthClient
@SpringBootApplication
public class LoanBusinessAdministrationApplication extends StarterWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoanBusinessAdministrationApplication.class, args);
	}

}
