import paho.mqtt.client as paho # importando al cliente
import time
import datetime
import sqlite3

import base64
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet


broker="broker.hivemq.com"

password_provided = 'WDrevvK8ZrPn8gmiNFjcOp2xovBr40TCwJlZOyI94IY='  # This is input in the form of a string
password = password_provided.encode()  # Convert to type bytes
print("*********passwprd********")
print(password)

 # Can only use kdf once

message = "hola mundo".encode()
phoneNumber = "937236187".encode()


def get_key(password):

    digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
    digest.update(password)
    return base64.urlsafe_b64encode(digest.finalize())

def encrypt(password, token):
    f = Fernet(get_key(password))
    return f.encrypt(bytes(token))

def decrypt(password, token):
    f = Fernet(get_key(password))
    return f.decrypt(bytes(token))



#key = get_key(password) 
print("*********key********")
#print(key)
token="asdasd"

print("*********encrypted********")
print(message)
message = encrypt(password, message)
print(message)
phoneNumber = encrypt(password, phoneNumber)
print(phoneNumber)
phoneNumber1 = decrypt(password, phoneNumber)
print(phoneNumber1)

out_publish = phoneNumber+"///*///"+message+"///*///"+password
print(out_publish)
client= paho.Client("client-001")
client.connect(broker)

print("publishing encrypted message", out_publish)
client.publish("in_message",out_publish)
