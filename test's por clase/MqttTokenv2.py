import paho.mqtt.client as paho # importando al cliente
import time
import datetime
import sqlite3

import base64
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet

############################################################
import serial
import RPi.GPIO as GPIO   
import time   
import os

#configuracion de hardware sim900
GPIO.setmode(GPIO.BOARD)
SERIAL_PORT = "/dev/ttyS0"
# Enable Serial Communication
port	 = serial.Serial(SERIAL_PORT, baudrate = 9600, timeout = 5)

token = ''
broker_address="broker.hivemq.com" #Servidor mosquito


# CONECCION CON LA BASE DE DATOS
dbase = sqlite3.connect('MQTTSIM900.db')#open database file
cursor = dbase.cursor()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # #SIM 900# # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def setupSIM900():
    print("Iniciando SIM900...")
    port.write('AT'+'\r')#comando AT = OK
    time.sleep(1)
    print(".")
    port.write('ATE0'+'\r')#comando AT0 = OK
    time.sleep(1)
    print(".")
    port.write('AT+CMGF=1'+'\r')#comando sim900 en modo mensajes
    time.sleep(1)
    print(".")
    port.write('AT+CNMI=2,1,0,0,0'+'\r')#comando sim900 recibir mensaje
    time.sleep(1)
    print("Configuracion Sim900 terminada...")
    


def send_messageSIM900(phoneNumber, message):
    port.write('AT+CMGS="'+phoneNumber+'"'+'\r')#comando para enviar mensajes de texto
    time.sleep(0.5)
    port.write(message+'\r')#mensaje
    time.sleep(0.5)
    port.write("\x1a")#ctrl + z
    time.sleep(2)
    print("mensaje Enviado")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # #Encryptacion de Mensages por password# # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def get_key(token):

    digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
    digest.update(token)
    return base64.urlsafe_b64encode(digest.finalize())

def encrypt(token, message):
    f = Fernet(get_key(token))
    return f.encrypt(bytes(message))

def decrypt(token, message):
    f = Fernet(get_key(token))
    return f.decrypt(bytes(message))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # #Gestion de la Base de datos# # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
def create_table():
    dbase.execute(''' CREATE TABLE IF NOT EXISTS SMS_SERVICE2(
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    phoneNumber TEXT NOT NULL,
    message TEXT NOT NULL,
    registrationDate TEXT NOT NULL,
    token TEXT NOT NULL) ''')

    print 'Table created'
    
def read_Data():
    # from math import *
    data = cursor.execute(''' SELECT * FROM SMS_SERVICE2 ORDER BY phoneNumber''')
    for record in data:
        print 'ID : '+str(record[0])
        print 'PHONE NUMBER : '+str(record[1])
        print 'MESSAGE : '+str(record[2])
        print 'REGISTRATION DATE : '+str(record[3])
        print 'TOKEN : '+str(record[4])+'\n'
        
def insert_Data(phoneNumber, message, registrationDate,token):
    data = "INSERT INTO SMS_SERVICE2(phoneNumber,message,registrationDate, token) VALUES(?,?,?,?)"
        
    dbase.execute(data, (phoneNumber,message,registrationDate,token))
    
    dbase.commit()
    print 'Record inserted'
    
def access_token(token):
    data = cursor.execute(''' SELECT token FROM TOKEN''')
    for record in data:
        aux = str(record[0])
        if(aux == token):
            return token.encode();
    return None  
    
    
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # #Gestion de Libreria MQTT # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

#Se Suscribe a los topicos asignados
def on_connect(client, userdata, flags, rc):
    print("Connected!", str(rc))
    time.sleep(1)
    client.subscribe("in_message")
    
#Se Muestra todos los mensajes recibidos desde MQTT
def on_message(client, userdata, message):
    
    
    messageDesc = message.payload.split('///*///') 
    time.sleep(1)

    print("phoneNumber = ",messageDesc[0])
    print("message = ",messageDesc[1])
    print("token = ",messageDesc[2])
    
    if(access_token(messageDesc[2])):
        token = access_token(messageDesc[2])
        phoneNumber = decrypt(token, messageDesc[0])
        messageSMS = decrypt(token, messageDesc[1])
        
        send_messageSIM900(str(phoneNumber),str(messageSMS))
        time.sleep(2)
        registrationDate = datetime.datetime.now()
        insert_Data(phoneNumber, messageSMS, registrationDate,token)
        
        read_Data()
    

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
setupSIM900()
print("termino de cargar el SIM900")
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #   
#conexcion con el servidor MQTT
print("Creating new Client")
client = paho.Client("SMS_Service")#Se crea el Cliente
client.connect(broker_address)
time.sleep(1)
#configuracion incial
print("Conectando a MQTT")
client.on_connect = on_connect
client.on_message = on_message
time.sleep(1)
print("MQTT configurado..")
print("")

create_table()
#insert_Data("1123456789","hellow bb","mananaxd","WDrevvK8ZrPn8gmiNFjcOp2xovBr40TCwJlZOyI94IY=")
#read_Data()


client.loop_forever()



